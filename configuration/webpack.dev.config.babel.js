import webpack from 'webpack';
import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';

const BUILD_DIR = path.resolve(__dirname, '../dist');
const APP_DIR = path.resolve(__dirname, '../src');

const BASE_EXTENSIONS = ['.js'];

export default {
  entry: [
    `${APP_DIR}/index.jsx`
  ],
  devServer: {
    contentBase: BUILD_DIR,
    compress: true,
    port: 8085,
    open: true,
  },
  output: {
    path: BUILD_DIR,
    filename: '[name]_bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.js[x]+/,
        loader: ['babel-loader']
      }
    ]
  },
  resolve: {
    extensions: [...BASE_EXTENSIONS, ...['.jsx']]
  },
  devtool: 'eval-source-map',
  plugins: [new HtmlWebpackPlugin({
    title: 'code_corn_time_app_components',
    template: 'src/index.html'
  })]
}
